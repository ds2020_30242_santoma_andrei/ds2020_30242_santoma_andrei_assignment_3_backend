package andrei;

import andrei.dtos.MedicationIntakeDTO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;
import andrei.services.PillDispenserService;

import java.util.List;
import java.util.UUID;

@SpringBootApplication
public class App {

    @Bean
    public HessianProxyFactoryBean hessianInvoker() {
        HessianProxyFactoryBean invoker = new HessianProxyFactoryBean();
        //invoker.setServiceUrl("http://localhost:8080/hellohessian");
        invoker.setServiceUrl("https://spring-app-andrei.herokuapp.com/hessian");
        invoker.setServiceInterface(PillDispenserService.class);
        return invoker;
    }

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

}
