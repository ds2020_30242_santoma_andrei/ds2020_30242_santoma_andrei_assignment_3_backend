package andrei.controllers;

import andrei.dtos.MedicationIntakeDTO;
import andrei.dtos.MedicationTakenDTO;
import andrei.services.PillDispenserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping
public class PillDispenserController {

    private final UUID PATIENT_UUID = UUID.fromString("95cbbb1a-1a7b-4c06-b59a-f31a4179a480");

    @Autowired
    private ApplicationContext context;

    @GetMapping(value = "/getmedications")
    public ResponseEntity<List<MedicationIntakeDTO>> getMedicationIntakeDTOS() {
        PillDispenserService pillDispenserService = context.getBean(PillDispenserService.class);
        return new ResponseEntity<>(pillDispenserService.getMedicationIntakes(PATIENT_UUID), HttpStatus.OK);
    }

    @PostMapping(value = "/medicationtaken")
    public ResponseEntity<Boolean> medicationTaken(@RequestParam("name") String name) {
        PillDispenserService pillDispenserService = context.getBean(PillDispenserService.class);
        pillDispenserService.medicationTaken(PATIENT_UUID, name, true);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    @PostMapping(value = "/medicationnottaken")
    public ResponseEntity<Boolean> medicationNotTaken(@RequestParam("name") String name) {
        PillDispenserService pillDispenserService = context.getBean(PillDispenserService.class);
        pillDispenserService.medicationTaken(PATIENT_UUID, name, false);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

}
